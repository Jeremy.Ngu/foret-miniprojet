public class Constante{
    public static final int tailleX = 16;
    public static final int tailleY = 10;
    public static final int TailleMaxPlante = 10;
	public static final int CroissancePlantes = 1;    
	public static final int TailleApparitionPlante = 4;
    public static final double reproductionHerbivores = 101;
    public static final double reproductionCarnivores = 101;
}
