import java.util.ArrayList;


public interface Predateur{
    public abstract void combattre(Animal a);
    public abstract boolean estProie(Animal a);
    public abstract int attaquer();
    public abstract void chasser(Animal a);
    public abstract void sentirProie(Foret f);
    public abstract void mangerAnimal(Animal a);
    public abstract Animal choisirProie(ArrayList<Animal> list);
}
