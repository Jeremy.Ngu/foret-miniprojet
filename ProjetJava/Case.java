import java.util.ArrayList;
import java.awt.*;

public class Case{
	private ArrayList<Animal> animaux; 
	private Terrain terrain;
	
	////////////////////////////CONSTRUCTEURS////////////////////////////
	
	public Case(Terrain t){
		animaux = new ArrayList<Animal>();
		terrain = t;
	}
	
	public Case(){
		this(new Terre());
	}
	
	public void initAleaCase(){
		double rand = Math.random();
		if(rand < 0.3334) terrain = new Terre();
		else if (rand < 0.6667){
			if (Math.random() > 0.5) terrain = new Plante();
			else terrain = new Plante(false);
		} 		
		else terrain = new Verdure();
	}
	
	////////////////////////////GETTEURS////////////////////////////
	
	public Terrain getTerrain(){
		return terrain;
	}
	
	public ArrayList<Animal> getAnimaux(){
		return animaux;
	}
	
	////////////////////////////SETTEURS////////////////////////////
	public void setTerrain(Terrain terrain){
		this.terrain = terrain;
	}
	public void setAnimaux(ArrayList<Animal> list){
		this.animaux = list ;
	}
	
	////////////////////////////UTILITAIRES////////////////////////////
	
	public Case clone(){
		Case CloneCase;
		CloneCase = new Case(terrain);
		
		CloneCase.animaux = new ArrayList<Animal>();
		
		return CloneCase;
	}
	
	public void marcherAnimaux(){
		for(Animal a : animaux){
			a.marcher();
		}
	}
	
	public void afficherAnimaux(Graphics2D g, int caseX, int caseY){
		int i = 0,j=0;
		for(Animal a : animaux){
			a.afficher(g,i*20+caseX,j*20+caseY);
			i++;
			if (i > 4){
				i = 0;
				j += 1;
			}
			
		}
	}
	
	public void ajouterAnimal(Animal a){
		animaux.add(a);
	}
	
	public void enleverAnimal(Animal a){
		animaux.remove(a);
	}
	
	public ArrayList<Animal> nettoyerMorts(ArrayList<Animal> list){	
		
		@SuppressWarnings("unchecked")
		ArrayList<Animal> listActualisee = (ArrayList<Animal>)list.clone();	
		
		for(Animal a : list){
			if(!a.enVie() || a.getEnergie() <= 0) listActualisee.remove(a);
		}
		return listActualisee;
	}
	
}
