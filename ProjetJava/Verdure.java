import javax.imageio.ImageIO;
import java.io.File;

public class Verdure extends Terrain{
    public Verdure(){

	try{
	    terrainImage = ImageIO.read(new File("verdure.png"));
	}catch(Exception e){e.printStackTrace();}
    }
	
    public Terrain actualiserTerrain(){
	if(Math.random() > 0.99)  return new Plante();
	
	return this;
    }
}
