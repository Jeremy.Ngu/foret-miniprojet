// Projet : La Forêt des animaux
// CREDITS :
// - Jérémy NGUYEN 
// - Nadym MALLEK

public class ForetMain{
	
	public static void main(String[] args){
		int nbCerf = 20;
		int nbLapin = 40;
		int nbLoup = 20;
		int nbOurs = 8;
		int nbRenard = 30;
		
		Foret testForet = new Foret(Constante.tailleX,Constante.tailleY);
		Fenetre testFenetre = new Fenetre("Foret Simulation", 1602,1040, testForet);	
		
		System.out.println(testForet.getLargeur());
		
		System.out.println(testForet.getLongueur());
		
		testForet.initForet();
		for(int i = 0; i < nbCerf ; ++i){
			testForet.addAnimal(new Cerf());
		}
		
		for(int i = 0; i < nbLapin ; ++i){
			testForet.addAnimal(new Lapin());
		}
		for(int i = 0; i < nbLoup ; ++i){
			testForet.addAnimal(new Loup());
		}
		for(int i = 0; i < nbOurs ; ++i){
			testForet.addAnimal(new Ours());
		}
		for(int i = 0; i < nbRenard ; ++i){
			testForet.addAnimal(new Renard());
		}		
		
		testFenetre.afficher();
		while (true){
			
			testForet = testForet.refreshForet();
			testFenetre.setForet(testForet);			
			testFenetre.afficher();
			try{
				Thread.sleep(1000);
			}catch(InterruptedException e){e.printStackTrace();}
		}
	}
}
