import javax.imageio.ImageIO;
import java.io.File;


public class Terre extends Terrain{
    public Terre(){
        try{
	    terrainImage = ImageIO.read(new File("terre.png"));
	}catch(Exception e){e.printStackTrace();}
    }
	
    public Terrain actualiserTerrain(){
	if(Math.random() > 0.992) return new Verdure();
	
	return this;
    }
}
